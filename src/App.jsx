import { useState, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import { Footer } from "./Components/Footer/Footer";
import { Header } from "./Components/Navbar/Header";
import { HomePage } from "./pages/Home";
import { Personajes } from "./pages/Personajes";

function App() {
  return (
    <div className="App" style={{ backgroundColor: "teal" }}>
      <Header />

      <Routes>
        <Route path="/" element={<HomePage />} />

        <Route path="/personaje/:personajeId" element={<Personajes />} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
