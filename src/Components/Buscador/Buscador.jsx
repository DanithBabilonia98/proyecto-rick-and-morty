export function Buscador({ InputValue, onInputChange }) {
  return (
    <div className="d-flex justify-content-end">
      <div className="mb-3 col-5">
        <input
          value={InputValue}
          onChange={(e) => onInputChange(e.target.value)}
          type="search"
          className="form-control"
          placeholder="Buscar personajes..."
        />
      </div>
    </div>
  );
}
