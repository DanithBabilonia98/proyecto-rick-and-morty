import { Episodio } from "./Episodio";

export function ListadoEpisodio({ episodios }) {
  return (
    <div className="row">
      {episodios.map((episodio) => (
        <Episodio key={episodio.data.id} {...episodio.data} />
      ))}
    </div>
  );
}
