export function Footer() {
  return (
    <div className="navbar-dark bg-dark">
      <p className="text-center text-white py-4 mb-0">
        Proyecto creado por Danith Babilonia Meza del CAR 4 con la asesoría de
        Osnaider Miranda Cáceres
      </p>
    </div>
  );
}
