import imgBanner from "../../Assets/ImagenBanner.jpg";
export function Banner() {
  return (
    <div>
      <img
        style={{ height: "1000px", width: "100%", objectFit: "cover" }}
        src={imgBanner}
        className="card-img"
        alt="Image"
      />
    </div>
  );
}
