import { PersonajeItem } from "./PersonajeItem";

import { useEffect, useState } from "react";
import axios from "axios";
import Pagination from "react-js-pagination";

export function ListadoPersonajes({ buscador }) {
  const [pagina, setPagina] = useState(1);
  const [personajes, setPersonajes] = useState(null);
  let personajesFiltrados = personajes?.results;

  let URL = "https://rickandmortyapi.com/api/character";
  let LIMIT = 42;

  function handlePageChange(nuevaPagina) {
    setPagina(nuevaPagina);
  }

  if (personajes && buscador) {
    personajesFiltrados = personajes.results.filter((personaje) => {
      let personajeTitleMayus = personaje.name.toLowerCase();
      let busquedaMayus = buscador.toLowerCase();

      return personajeTitleMayus.includes(busquedaMayus);
    });
  }

  useEffect(() => {
    axios.get(`${URL}?page=${pagina}`).then((respuesta) => {
      setPersonajes(respuesta.data);
    });
  }, [pagina]);

  return (
    <div className="row py-5">
      {personajesFiltrados
        ? personajesFiltrados.map((personaje) => {
            return <PersonajeItem key={personaje.id} {...personaje} />;
          })
        : "Cargando..."}
      <div className="d-flex justify-content-center">
        <Pagination
          itemClass="page-item"
          linkClass="page-link"
          activePage={pagina}
          itemsCountPerPage={LIMIT}
          totalItemsCount={826}
          onChange={handlePageChange}
        />
      </div>
    </div>
  );
}
