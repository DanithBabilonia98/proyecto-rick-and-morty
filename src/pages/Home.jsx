import { Banner } from "../Components/Navbar/Banner";
import { ListadoPersonajes } from "../Components/Personajes/ListadoPersonajes";
import { Buscador } from "../Components/Buscador/Buscador";
import { useState } from "react";

export function HomePage() {
  let [busqueda, setBusqueda] = useState("");

  return (
    <div>
      <Banner />

      <div className="px-5">
        <h1 className="py-4 text-center" style={{ color: "white" }}>
          Personajes Destacados
        </h1>

        <Buscador InputValue={busqueda} onInputChange={setBusqueda} />

        <ListadoPersonajes buscador={busqueda} />
      </div>
    </div>
  );
}
