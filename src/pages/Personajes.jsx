import { useEffect, useState } from "react";

import { BannerDetallePersonaje } from "../Components/Personajes/BannerDetallePersonaje";
import { ListadoEpisodio } from "../Components/Episodio/ListadoEpisodio";
import axios from "axios";
import { useParams } from "react-router-dom";

export function Personajes() {
  let { personajeId } = useParams();
  let [personaje, setPersonaje] = useState(null);
  let [episodios, setEpisodios] = useState(null);

  useEffect(() => {
    axios
      .get(`https://rickandmortyapi.com/api/character/${personajeId}`)
      .then((respuesta) => {
        setPersonaje(respuesta.data);
      });
  }, []);

  useEffect(() => {
    if (personaje) {
      let peticionesEpisodios = personaje.episode.map((episodio) => {
        return axios.get(episodio);
      });

      Promise.all(peticionesEpisodios).then((respuestas) => {
        setEpisodios(respuestas);
      });
    }
  }, [personaje]);

  return (
    <div className="p-5">
      {personaje ? (
        <div>
          <BannerDetallePersonaje {...personaje} />

          <h2 className="py-4">Episodios</h2>
          {episodios ? (
            <ListadoEpisodio episodios={episodios} />
          ) : (
            <div>Cargando episodios...</div>
          )}
        </div>
      ) : (
        <div>Cargando...</div>
      )}
    </div>
  );
}
